# devuan-mirmon
mirmon for devuan mirrors

NOT WORKING : mirror age. in contrast to debian, no stats file (project/trace/ftp-master.debian.org) in mirrors. 
now, just using amprolla.txt for mirror age, with first date shown, ~250 days earlier...

GET STARTED :

(as_root)	
install mirmon : apt install mirmon -y

(as_user)
clone repo : git clone https://git.devuan.org/xinomilo/devuan-mirmon.git
create initial state file : touch devuan-mirmon/state 
copy conf : cp devuan-mirmon/devuan_mirmon.conf  devuan-mirmon/mirmon.conf
adjust devuan-mirmon/mirmon.conf with correct path for mirror_list, state and web_page files.

note: mirmon by default runs in cron.hourly as root. so, comment out everything in 
/etc/cron.hourly/mirmon and add a user cron instead : crontab -e
add a line like :
*/30 * * * * /usr/bin/mirmon -q -get update -c /path/to/devuan-mirmon/mirmon.conf

note: run cron command manually first, to check validity of path, errors, etc.

apache virtualhost alias for icons : Alias /icons/ /usr/share/mirmon/icons/
(or you can just cp -r /usr/share/mirmon/icons in web_page directory.


demo : https://status.stinpriza.org/mirmon/

mirmon : https://www2.projects.science.uu.nl/ictbeta/mirmon/
